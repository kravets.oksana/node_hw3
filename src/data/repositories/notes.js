const models = require('../models/models');

class Note {
  note = models.Note;

  async createNote({ text, userId }) {
    const newNote = await this.note.create({
      text,
      userId,
      completed: false,
    });
    return newNote;
  }

  async getById(id) {
    const note = await this.note.findById(id);
    return note;
  }

  async updateById(param, value, id) {
    const noteToUpdate = await this.note
      .findById(id)
      .select();
    noteToUpdate[param] = value;
    await noteToUpdate.save();
  }

  async updateStatus(param, value, id) {
    const noteToUpdate = await this.note
      .findById(id)
      .select();
    noteToUpdate[param] = value;
    await noteToUpdate.save();
  }

  async delete(id) {
    await this.note.findByIdAndDelete(id);
  }

  async noteExists(id) {
    const userExists = await this.note.exists({
      id,
    });
    return userExists;
  }

  async getAllUsersNotes(id, offset, limit) {
    const notes = await this.note
      .where({ userId: id })
      .skip(offset)
      .limit(limit);
    return notes;
  }
}

module.exports = Note;
