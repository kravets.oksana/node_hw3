const express = require('express');
const mongoose = require('mongoose');
const routes = require('./api/routes');

require('dotenv').config();

const { PORT, DATABASE_URL } = process.env;
const app = express();
mongoose.connect(
  DATABASE_URL,
  () => {
    console.log('connected');
  },
  (e) => console.log(e),
);

app.use(express.json());
routes(app);

app.listen(PORT, () => console.log(`Express is listening at http://localhost:${PORT}`));
