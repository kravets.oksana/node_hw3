const bcrypt = require('bcryptjs');
const repositories = require('../../data/repositories/repositories');

const userRepository = new repositories.UserRepository();

module.exports.getUser = async (req, res, next) => {
  const { userId } = req.user;
  const user = await userRepository.getUserById(userId);
  res.status(200).json({user:user});

  next();
};

module.exports.deleteUser = async (req, res, next) => {
  const { userId, username } = req.user;

  try {
    const userExists = await userRepository.userExists(
      username,
    );

    if (userExists) {
      await userRepository.deleteUser(userId);
      res.status(200).json({
        message: 'Success',
      });
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};

module.exports.updateUser = async (req, res, next) => {
  const { userId, username } = req.user;
  const { newPassword } = req.body;
  try {
    const userExists = await userRepository.userExists(
      username,
    );

    const hashPass = await bcrypt.hash(newPassword, 10);

    if (userExists) {
      await userRepository.updateUser(
        'password',
        hashPass,
        userId,
      );
      res.status(200).json({
        message: 'Success',
      });
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};
