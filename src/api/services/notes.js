const repositories = require('../../data/repositories/repositories');

const notesRepository = new repositories.NotesRepository();
const userRepository = new repositories.UserRepository();

module.exports.createNote = async (req, res, next) => {
  const { text } = req.body;
  const { userId, username } = req.user;

  try {
    const userExists = await userRepository.userExists(
      username,
    );

    if (userExists) {
      const newNote = await notesRepository.createNote({
        text,
        userId,
      });
      res.status(200).send({
        message: "Success"
      });
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};

module.exports.getById = async (req, res, next) => {
  const { id } = req.params;
  const { username } = req.user;

  try {
    const userExists = await userRepository.userExists(
      username,
    );

    const noteExists = await notesRepository.noteExists(id);

    if (userExists && noteExists) {
      const note = await notesRepository.getById(id);
      res.status(200).send({note: note});
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};

module.exports.getAll = async (req, res, next) => {
  const { userId, username } = req.user;

  const { offset } = req.query;
  const { limit } = req.query;

  try {
    const userExists = await userRepository.userExists(
      username,
    );

    if (userExists) {
      const notes = await notesRepository.getAllUsersNotes(
        userId,
        offset,
        limit,
      );
      res.status(200).send({
        offset: offset,
        limit: limit,
        count: notes.length,
        notes: notes
      });
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};

module.exports.updateById = async (req, res, next) => {
  const { id } = req.params;
  const { text } = req.body;

  try {
    const { username } = req.user;
    const userExists = await userRepository.userExists(
      username,
    );

    const noteExists = await notesRepository.noteExists(id);
    if (userExists && noteExists) {
      const note = await notesRepository.updateById(
        'text',
        text,
        id,
      );
      res.status(200).send({
        message: "Success"
      });
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};

module.exports.updateStatus = async (req, res, next) => {
  const { id } = req.params;
  const { username } = req.user;
  try {
    const userExists = await userRepository.userExists(
      username,
    );
    const noteExists = await notesRepository.noteExists(id);

    if (userExists && noteExists) {
      const note = await notesRepository.updateById(
        'completed',
        true,
        id,
      );
      res.status(200).send({
        message: "Success"
      });
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};

module.exports.delete = async (req, res, next) => {
  const { id } = req.params;
  const { username } = req.user;

  try {
    const userExists = await userRepository.userExists(
      username,
    );
    const noteExists = await notesRepository.noteExists(id);

    if (userExists && noteExists) {
      await notesRepository.delete(id);
      res.status(200).send({
        message: "Success"
      });
    } else {
      res.status(404).json({
        message: 'User does not exist',
      });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};
