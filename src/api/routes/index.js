const usersRoutes = require('./users');
const authRoutes = require('./auth');
const notesRoutes = require('./notes');

module.exports = (app) => {
  app.use('/api/users', usersRoutes);
  app.use('/api/notes', notesRoutes);
  app.use('/api/auth', authRoutes);
};
